<?php
/**
 * Copyright © Kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Kowal_Blog', __DIR__);

