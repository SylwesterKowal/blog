<?php
/**
 * Copyright © Kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\Blog\Model\Product\Type;

class Post extends \Magento\Catalog\Model\Product\Type\Virtual
{

    const TYPE_ID = 'post';

    /**
     * {@inheritdoc}
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
        // method intentionally empty
    }
}

